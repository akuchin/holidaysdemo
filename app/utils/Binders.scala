package utils

import java.time.LocalDate

import play.api.mvc.PathBindable.Parsing
import utils.Converters._

import scala.language.implicitConversions

object Binders {

  implicit object bindableLocalDate extends Parsing[LocalDate](
    _.toLocalDate, _.toString, (key: String, e: Exception) => "Cannot parse parameter %s as Date: %s".format(key, e.getMessage)
  )

}
