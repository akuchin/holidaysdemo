import java.time.Clock

import com.google.inject.{AbstractModule, Provides}
import play.api.{Configuration, Environment}
import services.calendar.{BusinessCalendarGenerator, CalendarService, SimpleCalendarService}
import services.users._
import services.{EmailService, SmtpConfig}

class Module(environment: Environment, config: Configuration) extends AbstractModule {

  override def configure() = {
    // Use the system clock as the default implementation of Clock
    bind(classOf[Clock]).toInstance(Clock.systemDefaultZone)

    bind(classOf[UserService]).to(classOf[InMemoryUserService])
    bind(classOf[EmailService])
    bind(classOf[BusinessCalendarGenerator])
    bind(classOf[TokenService]).to(classOf[InMemoryTokenService])
    bind(classOf[BillingService]).to(classOf[InMemoryBillingService])
    bind(classOf[CalendarService]).to(classOf[SimpleCalendarService])
  }

  @Provides def smtpConfig(): SmtpConfig = {
    SmtpConfig(
      host = config.getString("smtp.host").get,
      port = config.getInt("smtp.port").getOrElse(25),
      username = config.getString("smtp.username").get,
      password = config.getString("smtp.password").get
    )
  }
}
