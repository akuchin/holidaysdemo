package controllers

import javax.inject._

import play.api.libs.json.{Json, OWrites}
import play.api.mvc._
import services.users.UserService
import utils.Converters._
import utils.ErrorDto

@Singleton
class UsersControllers @Inject()(userService: UserService) extends Controller {

  implicit val errorWrites: OWrites[ErrorDto] = Json.writes[ErrorDto]

  def register(email: Option[String]) = Action {
    email.filter(mail => mail.endsWith("@epam.com"))
      .map(mail => {
        userService.register(mail, mail.encrypt)
        Ok("Ok")
      })
      .getOrElse(BadRequest(Json.toJson(ErrorDto("0", "Invalid email try @epam.com"))))

  }

}
