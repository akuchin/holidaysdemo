package controllers

import java.time.LocalDate
import javax.inject._

import controllers.actions.{Billable, OAuthAuthentication}
import play.api.libs.json._
import play.api.mvc._
import services.calendar.{CalendarService, DayOff}
import services.users.{BillingService, TokenService}

@Singleton
class CalendarControllers @Inject()(
                                     calendarService: CalendarService,
                                     val billingService: BillingService,
                                     val tokenService: TokenService)
  extends Controller with OAuthAuthentication with Billable {

  implicit val dayOffWrites: OWrites[DayOff] = Json.writes[DayOff]

  def getDayOffs(from: LocalDate, to: LocalDate) = OAuthAction {
    BillingAction {
      validate(from, to) match {
        case Some(error) => BadRequest(error)
        case _ => Ok(Json.obj("days" -> calendarService.holidays(from, to)))
      }
    }
  }

  private def validate(start: LocalDate, end: LocalDate): Option[String] = {
    (start, end) match {
      case (from, to) if from.isAfter(to) => Some("from > to")
      case (from, to) if LocalDate.ofEpochDay(0).isAfter(from) => Some("we dont care of the past")
      case (from, to) if to.getYear - from.getYear > 10 => Some("to long query")
      case _ => None
    }
  }
}
