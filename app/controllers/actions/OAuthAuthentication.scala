package controllers.actions

import play.api.mvc.Security.AuthenticatedRequest
import play.api.mvc.{RequestHeader, Results, _}
import services.users.TokenService

import scala.concurrent.Future

trait OAuthAuthentication {

  def tokenService: TokenService

  def getToken(req: RequestHeader): Option[String] = {
    req.headers.get("Authorization")
      .filter(_.startsWith("Bearer "))
      .map(auth => auth.replaceAll("Bearer ", ""))
  }

  case class OAuthAction[A](action: Action[A]) extends Action[A] {

    def apply(request: Request[A]): Future[Result] = {
      getToken(request).flatMap(token => tokenService.findUser(token)) match {
        case Some(user) => action(new AuthenticatedRequest(user, request))
        case _ => Future.successful(Results.Unauthorized("Authentication failed"))
      }
    }

    lazy val parser = action.parser
  }


  //  object AuthenticatedAction extends AuthenticatedBuilder(
  //    req => getToken(req).flatMap(token => tokenService.findUser(token)),
  //    reqHead => Results.Unauthorized("Authentication failed")
  //  )

}