package services

import javax.inject._

import akka.actor.ActorSystem
import org.apache.commons.mail.{DefaultAuthenticator, Email, SimpleEmail}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

@Singleton
class EmailService @Inject() (smtpConf: SmtpConfig, actorSystem: ActorSystem)(implicit exec: ExecutionContext)  {

  def sendEmail(email: String, subj: String, message: String) {
    actorSystem.scheduler.scheduleOnce(1.second) { internalSendEmail(email, subj, message)}
  }

  private def internalSendEmail(to: String, subj: String, message: String) {
    val email: Email = new SimpleEmail
    email.setHostName(smtpConf.host)
    email.setSmtpPort(smtpConf.port)
    email.setAuthenticator(new DefaultAuthenticator(smtpConf.username, smtpConf.password))
    email.setSSLOnConnect(true)
    email.setFrom(smtpConf.username)
    email.setSubject(subj)
    email.addTo(to)
    email.setMsg(message)
    email.send
  }

}

case class SmtpConfig(host: String, port: Int = 25, username:String, password: String)

