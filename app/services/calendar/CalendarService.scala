package services.calendar

import java.time.LocalDate
import javax.inject._

import play.api.Configuration
import utils.Converters._

trait CalendarService {
  def holidays(from: LocalDate, to: LocalDate): Seq[DayOff]
}

@Singleton
class SimpleCalendarService @Inject()(generator: BusinessCalendarGenerator, config: Configuration) extends CalendarService {

  private val dayOffs = generator.getCalendar(LocalDate.ofEpochDay(0).getYear, 2099)

  def holidays(start: LocalDate, end: LocalDate): Seq[DayOff] = {
    config.getInt("hitch").filter(_ > 0).map(_ * 1000).foreach(Thread.sleep(_))

    dayOffs
      .from(start toDayOff)
      .to(end toDayOff)
      .to[Seq]
  }
}
